<?php
include("Pohlavi.php");

class Osoba {
    public function __construct($id, $jmeno, $prijmeni, $pohlavi, $datum_nar)
    {
        $this->id = $id;
        $this->jmeno = $jmeno;
        $this->prijmeni = $prijmeni;

        //PHP bohužel nemá výčtový typ ENUM, tak jsem to vyřešil takto...
        if(strtolower($pohlavi) == Pohlavi::Muz || strtolower($pohlavi) == Pohlavi::Zena) {
            $this->pohlavi = strtolower($pohlavi);
        }
        else {
            $this->pohlavi = NULL;
        }
        
        if($datum_nar instanceof DateTime) {
            $this->datum_nar = $datum_nar;
        }
        else {
            $datum_nar = preg_replace('/[\x00-\x1F\x7F]/u', '', $datum_nar); //odstranění nežádoucích mezer a tabulátorů
            $this->datum_nar = DateTime::createFromFormat('d.m.Y', $datum_nar);
        }
    }

    
    //proměnné budou private, aby nemohly být přepisovány mimo tuto třídu
    private $id, $jmeno, $prijmeni, $pohlavi, $datum_nar;

    /*
        Pro získání informací z této třídy použiji gettery ve formě funkcí,
        které budou vracet obsah proměnných
    */

    /**
     * @return string ID osoby
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string křestní jméno osoby
     */
    public function getJmeno() {
        return $this->jmeno;
    }

    /**
     * @return string příjmení osoby
     */
    public function getPrijmeni() {
        return $this->prijmeni;
    }

    /**
     * @return Pohlavi pohlaví osoby
     */
    public function getPohlavi() {
        return $this->pohlavi;
    }

    /**
     * @return DateTime datum narození osoby
     */
    public function getDatumNarozeni() {
        return $this->datum_nar;
    }

    /**
     * @return int délka života v dnech
     */
    public function getDelkaZivota() {
        if($this->datum_nar != NULL) {
            return (int)(new DateTime("now"))->diff($this->getDatumNarozeni())->format("%a");
        }
    }

}