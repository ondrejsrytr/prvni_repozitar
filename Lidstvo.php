<?php
include("Osoba.php");
class Lidstvo implements Iterator {
    public function __construct()
    {
        $this->index = 0;
        $this->osoby = array();
        $this->pocet_zen = 0;
        $this->pocet_muzu = 0;
        $this->nacistOsobyZeSouboru();
    }

    private $index;
    private $osoby;

    private $pocet_zen;
    private $pocet_muzu;

    /**
     * Načte seznam osob ze souboru
     * @param string $jmeno_souboru Jméno souboru s daty
     */
    public function nacistOsobyZeSouboru($jmeno_souboru = "osoby.csv") {
        $obsahCSVsouboru = file_get_contents($jmeno_souboru);
        if($obsahCSVsouboru) {
            $this->osoby = array();
            $this->pocet_zen = 0;
            $this->pocet_muzu = 0;
            $osoby = explode("\n", $obsahCSVsouboru);
            foreach($osoby as $i => $osoba) {
                if(empty(trim($osoba)) || trim($osoba) == "\n") {
                    unset($osoby[$i]);
                }
                else {
                    $radek = explode(";", $osoba);
                    $i_id= $radek[0];
                    $i_jmeno = $radek[1];
                    $i_prijmeni = $radek[2];
                    $i_pohlavi = strtolower($radek[3]);
                    $i_datnarozeni = $radek[4];

                    if($i_pohlavi == Pohlavi::Muz) $this->pocet_muzu++;
                    else if($i_pohlavi == Pohlavi::Zena) $this->pocet_zen++;
                    array_push($this->osoby, new Osoba($i_id, $i_jmeno, $i_prijmeni, $i_pohlavi, $i_datnarozeni));
                }
            }
        }
        else {
            throw new Exception("Chyba při čtení souboru '". $jmeno_souboru."'");
        }
    }

    /**
     * @param int $id ID osoby
     * @return Osoba|NULL Vrátí instanci třídy Osoba, která odpovídá parametru ID. Pokud se žádná nenajde, vrátí NULL
     */
    public function getOsobaById($id) {
        $return_val = NULL;
        foreach($this->osoby as $osoba) {
            if($osoba->getId() == $id) {
                $return_val = $osoba;
                break;
            }
            else continue;
        }
        return $return_val;
    }

    /**
     * @return float Procentuální zastoupení mužů 0-100%
     */
    public function getProcZastoupeniMuzu() {
        return $this->pocet_muzu / ($this->pocet_muzu + $this->pocet_zen) * 100;
    }

    /* 
        Funkce pro procházení třídy cyklem foreach
    */
    public function rewind(){
        $this->index = 0;
    }
    public function current() {
        $k = array_keys($this->osoby);
        $var = $this->osoby[$k[$this->index]];
        return $var;
    }
    public function key() {
        $k = array_keys($this->osoby);
        $var = $k[$this->index];
        return $var;
    }
    public function next() {
        $k = array_keys($this->osoby);
        if (isset($k[++$this->index])) {
            $var = $this->osoby[$k[$this->index]];
            return $var;
        }
        else {
            return false;
        }
    }
    public function valid()
    {
        $k = array_keys($this->osoby);
        $var = isset($k[$this->index]);
        return $var;
    }
}