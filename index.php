<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>

    <?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    include("Lidstvo.php");

    $lidstvo = new Lidstvo();

    ?>

    <div>
        Procentuální zastoupení mužů: <?= $lidstvo->getProcZastoupeniMuzu() ?>%<br>
        Test funkce getOsobaById(123): <?= $lidstvo->getOsobaById(123)->getJmeno() ?>
        
    </div>
    <br>
    <div>
        <table border="1">
            <tr>
                <td>ID</td>
                <td>Jméno</td>
                <td>Příjmení</td>
                <td>Pohlaví</td>
                <td>Datum narození</td>
                <td>Délka života ve dnech</td>
            </tr>
            <?php
            foreach($lidstvo as $osoba)
            {
                echo "<tr>";
    
                echo "<td>".$osoba->getId()."</td>";
                echo "<td>".$osoba->getJmeno()."</td>";
                echo "<td>".$osoba->getPrijmeni()."</td>";
                echo "<td>";
                switch($osoba->getPohlavi()) {
                    case Pohlavi::Muz: echo "Muž"; break;
                    case Pohlavi::Zena: echo "Žena"; break;
                }
                echo "</td>";
                echo "<td>".$osoba->getDatumNarozeni()->format("d. m. Y")."</td>";
                echo "<td>".$osoba->getDelkaZivota()."</td>";
    
                echo "</tr>";
            }
            ?>
        </table>
    </div>
    </body>
</html>